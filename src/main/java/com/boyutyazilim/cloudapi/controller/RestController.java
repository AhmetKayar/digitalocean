package com.boyutyazilim.cloudapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("v1")
public class RestController {

    @GetMapping("value")
    public ResponseEntity<String> value(){

        return ResponseEntity.ok("Hello Word");
    }
}
